﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

namespace DiscordBot.Core.Commands
{
    public class TextCommands : ModuleBase<SocketCommandContext>
    {
        [Command("hello"), Alias("helloworld","world"), Summary("Hello world command")] 
        public async Task HelloCommand()
        {
            await Context.Channel.SendMessageAsync("Hello World");
        }

        [Command("Embeds"), Summary("Embed test")]
        public async Task embed()
        {
            EmbedBuilder embed = new EmbedBuilder();  //build a embeded look. 
            embed.WithAuthor("Test embed", Context.User.GetAvatarUrl());


            await Context.Channel.SendMessageAsync("", false, embed.Build());
        }

        [Command ("Ping"), Summary("Sends a pong response with latency")]
        public async Task Pingbot()
        {
            await Context.Channel.SendMessageAsync("Pong " + Context.User.Mention + " at " + Context.Client.Latency + "ms");
        }

        [Command("link"), Summary("Link RLTracker rank and changes user Name and role (?link [platform] [profileID]")]
        public async Task LinkUser([Remainder] string Input)
        {
           int DelimiterPos = Input.IndexOf(" ");              
               
            DelimiterPos = Input.IndexOf(" ");
            string platform = Input.Substring(0, DelimiterPos).ToUpper();

            string gamerID = Input.Substring(DelimiterPos + 1);

   
           // Context.Guild.GetUser(Context.User.Id).Nickname.Replace("", "[" + platform + "]" + "Elf_Jnr");

            await Context.Channel.SendMessageAsync(platform + " " + gamerID);
            
        }
    }
}
