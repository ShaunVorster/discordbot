﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Reflection;

namespace DiscordBot
{
    class Program
    {
        private DiscordSocketClient Client;
        private CommandService Commands;

        static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        private async Task MainAsync()
        {
            Client = new DiscordSocketClient(new DiscordSocketConfig
            {
                LogLevel = LogSeverity.Debug
            });



            Commands = new CommandService(new CommandServiceConfig
            {
                CaseSensitiveCommands = false,
                DefaultRunMode =RunMode.Async,
                LogLevel = LogSeverity.Debug            
            });

            Client.MessageReceived += Client_MessageReceived;           //commands for bot
            await Commands.AddModulesAsync(Assembly.GetEntryAssembly());

            Client.Ready += Client_Ready;
            Client.Log += Client_Log;

            string Token = "NDQ1NTExMTM0NDkwMDAxNDA4.DhzdLg.nlKU-R_LjTPI5DzxICuFX110DsM";  //bot token
            await Client.LoginAsync(TokenType.Bot, Token);                                  // log in and start bot
            await Client.StartAsync();

            await Task.Delay(-1);  //keep bot alive, won't stop
        }

        private async Task Client_Log(LogMessage Message)  //text for the console
        {
            Console.WriteLine($"{DateTime.Now} at {Message.Source}] {Message.Message}");
        }

        private async Task Client_Ready()  // bot status
        {
            await Client.SetGameAsync("This bot is crazy");
        }

        private async Task Client_MessageReceived(SocketMessage clientMessage)  // Configure the commands for the bot
        {
            var Message = clientMessage as SocketUserMessage;
            var Contex = new SocketCommandContext(Client, Message);

            if (Contex.Message == null || Contex.Message.Content == "") return;  // ensure message has content
            if (Contex.User.IsBot) return;  //bot should not send commands

            int ArgPos = 0; //prefix space 
            if (!(Message.HasStringPrefix("?", ref ArgPos) || Message.HasMentionPrefix(Client.CurrentUser, ref ArgPos))) return;

            var result = await Commands.ExecuteAsync(Contex, ArgPos);
            if (!result.IsSuccess)
                Console.WriteLine($"{DateTime.Now} at Commands] Somthing went wrong with the command. Text: {Contex.Message.Content} | Error: {result.ErrorReason}");
        }
    }

}
